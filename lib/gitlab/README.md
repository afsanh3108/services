# GitLab integration/service

Latest Webhook payload examples that you can use to update `examples/*`, https://gitlab.com/help/user/project/integrations/webhooks


### Testing

```
npm run mocha -- lib\gitlab\test\index.js
```
