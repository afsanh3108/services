In order to receive repl.it classroom notifications, you will need to configure
[webhooks](https://repl.it/site/docs/classrooms/webhooks) on repl.it:

1. Go to your Teacher tab on [repl.it](https://repl.it/teacher)
3. Decide which classroom notifications you wish to be notified on
2. Click on three vertical dots in the bottom right corner of that classroom's icon
3. Click "Webhooks"
4. Add your Gitter webhook URL in the dialog box and click "Save"


