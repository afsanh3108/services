var assert = require('assert');
var heroku = require('../..').heroku;
var parse = heroku.parse;
var examples = heroku.examples;

describe('Heroku', function() {

  it('should generate a completed', function() {
    var payload = examples['completed'];

    var response = parse(payload.headers, payload.body);

    assert.equal(response.message, 'Heroku [iodide-server] deployed b3263eb2');
    assert.equal(response.icon, 'logo');
    assert.equal(response.errorLevel, 'normal');
  });

  it('not current should not generate an event', function() {
    var payload = examples['completed-not-current'];

    var response = parse(payload.headers, payload.body);

    assert.equal(response, undefined);
  });

});
